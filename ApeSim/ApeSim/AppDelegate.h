//
//  AppDelegate.h
//  ApeSim
//
//  Created by Thomas Barbalet on 9/20/19.
//  Copyright © 2019 Thomas Barbalet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

